import copy
import torch

from os.path import join
from tqdm.notebook import tqdm

import src.config as cfg
import src.utils as utils
import src.train_config as train_config


class EfficientNetModel:
    def __init__(self, version):

        self.model = utils.load_model(version=version)
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    def load(self, model_name):

        self.model.load_state_dict(torch.load(join(cfg.MODEL_DIR, model_name)))
        self.model.eval()

    def save(self, model_name):

        torch.save(self.model.state_dict(), join(cfg.MODEL_DIR, model_name))

    def train(self, epochs, dataloaders, loss_function, optimizer):
        """
        function used to train model on given dictionary of pytorch dataloaders -
        with keys "train" and "test", for the stages of training
        """
        val_acc_history = []

        # remember best model so far
        best_model_wts = copy.deepcopy(self.model.state_dict())
        best_acc = 0.0

        for epoch in range(epochs):

            print(f"Epoch {epoch + 1} of {epochs}")
            print("-" * 10)

            for phase in ["train", "test"]:

                if phase == "train":
                    # set model to training mode
                    self.model.train()
                else:
                    # set model to evaluate mode
                    self.model.eval()

                # start counting phase loss
                running_loss = 0.0
                running_corrects = 0

                for batch in tqdm(dataloaders[phase]):

                    # load data to device
                    inputs = batch["image"].to(self.device)
                    labels = batch["target"].to(self.device)

                    # reset optimizer
                    optimizer.zero_grad()

                    # generate outputs of network on batch images and generate loss
                    outputs = self.model(inputs)
                    loss = loss_function(outputs, labels)

                    # predict the labels of batch images
                    _, predictions = torch.max(outputs, 1)

                    if phase == "train":
                        loss.backward()
                        optimizer.step()

                    # calculate statistics
                    running_loss += loss.item() * inputs.size(0)
                    batch_acc = (
                        torch.sum(predictions == labels.data).item()
                        / train_config.batch_size
                    )
                    running_corrects += batch_acc

                epoch_loss = running_loss / len(dataloaders[phase])
                epoch_acc = running_corrects / len(dataloaders[phase])

                print(f"{phase} Loss: {epoch_loss} Acc: {epoch_acc}")

                # deep copy the model
                if phase == "test" and epoch_acc > best_acc:
                    best_acc = epoch_acc
                    best_model_wts = copy.deepcopy(self.model.state_dict())
                    self.save(f"best")
                if phase == "test":
                    val_acc_history.append(epoch_acc)

        # load best model weights
        self.model.load_state_dict(best_model_wts)

    def __call__(self, batch):

        return self.model(batch)
