import matplotlib.pyplot as plt
import numpy as np
import os
import pickle
import torch

from abc import ABC, abstractmethod
from torchvision import transforms

import src.config as cfg
import src.utils as utils


class Augment(ABC):
    def check_augmentation(self, batch=None):

        if batch is None:
            batch = utils.get_sample_images(amount=5)

        augmented = [self.augment_image(image) for image in batch]

        rows = len(batch)
        columns = 2

        fig = plt.figure(figsize=(14, rows * 7))

        for i, before, after in zip(range(rows), batch, augmented):

            fig.add_subplot(rows, columns, 2 * i + 1)
            plt.title("before", size=35)
            plt.imshow(before)

            fig.add_subplot(rows, columns, 2 * i + 2)
            plt.title("after", size=35)
            plt.imshow(after)

    @abstractmethod
    def augment_image(self, image):
        pass

    def __call__(self, image):
        return self.augment_image(image)


class CustomAugment(Augment):
    pass


class ImgaugAugment(Augment):
    def __init__(self, imgaug_augment):
        self.augmenter = imgaug_augment

    def augment_image(self, image):
        return self.augmenter(images=np.array([image]))[0]


class AugmentList:
    def __init__(self, augments):
        self.augments = augments

        self.mean, self.std = pickle.load(
            open(os.path.join(cfg.DATA_DIR, "images_mean_std.pkl"), "rb")
        )

    def check_augmentation(self, batch=None):

        if batch is None:
            batch = utils.get_sample_images(amount=5)

        augmented = [self.augment_image(image) for image in batch]

        rows = len(batch)
        columns = 2

        fig = plt.figure(figsize=(14, rows * 7))

        for i, before, after in zip(range(rows), batch, augmented):

            fig.add_subplot(rows, columns, 2 * i + 1)
            plt.title("before", size=35)
            plt.imshow(before)

            fig.add_subplot(rows, columns, 2 * i + 2)
            plt.title("after", size=35)
            plt.imshow(after)

    def __call__(self, image):

        """
        final function that transo
        """

        image = self.augment_image(image)

        image = torch.tensor(image.transpose((2, 0, 1))).float()

        image = transforms.Normalize(self.mean, self.std)(image)

        return image

    def augment_image(self, image):

        for augment in self.augments:
            image = augment(image)

        return image

    def __len__(self):
        return len(self.augments)

    def __getitem__(self, idx):
        return self.augments[idx]
