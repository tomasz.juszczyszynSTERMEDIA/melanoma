from torch.utils.data import Dataset, DataLoader

import src.utils as utils
import src.train_config as train_config


class PytorchDataset(Dataset):
    def __init__(self, image_data):
        """
        Args:
            image_data (pandas.DataFrame): csv file with annotations, with columns
            "image_path" - path to the file
            "target" - self explanatory
        """

        self.negative = image_data[image_data["target"] == 0].reset_index()
        self.positive = image_data[image_data["target"] == 1].reset_index()
        self.size = max(len(self.positive), len(self.negative))

    def __len__(self):
        return 2 * self.size

    def __getitem__(self, idx):

        if idx < self.size:
            data = self.negative
            target = 1
        else:
            data = self.positive
            idx = (idx - self.size) // len(self.positive)
            target = 0

        image_path = data.loc[idx, "image_path"]
        image = utils.get_image(image_path)

        # apply augments
        image = train_config.augments(image)

        sample = [image, target]
        return sample


def create_dataloaders(datasets):
    """
    Function to create dataloaders from datasets
    Args:
        datasets: dictionary with keys from ["train", "test", "valid"], and values
            of pandas dataframes with information about images

        batch_size: size of batch in dataloaders

        shuffle: dict with keys ["train","valid","test"] and boolean values determinig
        if to shuffle dataset during training

        num_workers: number of workers in dataloaders
    """

    loaders = {
        stage: DataLoader(
            PytorchDataset(datasets[stage]),
            batch_size=train_config.batch_size,
            shuffle=train_config.shuffle[stage],
        )
        for stage in datasets
    }

    return loaders
