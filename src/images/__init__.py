import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pickle

from os.path import join
from sklearn.model_selection import train_test_split

import src.config as cfg
import src.utils as utils
import src.train_config as train_config


class ImageUtils:
    """
    Class to handle actions with images - getting images list, image samples,
    ploting images, creating pandas DataFrames to train
    """

    def __init__(self, stage="train"):

        self.stage = stage
        self.data = pd.read_csv(join(cfg.DATA_DIR, f"{self.stage}.csv"))

    def _filter_data(
        self, target=None, hairy=None, amount=8, random=True, filter_func=None, **kwargs
    ):
        """
        Function to filter self.data basing on criteria given in arguments
        For more complex use please use filter_func - a callable that takes
        pd.DataFrame and returns filtered one.
        """

        data = self.data.copy()

        if filter_func:
            return filter_func(data)

        if target:
            data = data[data["target"] == target]

        if hairy:
            data = data[data["hairy"] == hairy]

        if random:
            data = data.sample(amount)
        else:
            data = data.head(amount)

        return data

    def _plot_from_data(self, data, label="target", **kwargs):

        amount = data.shape[0]
        columns = 4
        rows = amount // columns + 1

        fig = plt.figure(figsize=(columns * 7, rows * 7))

        for i, image_path, label in zip(
            range(1, amount + 1), data["image_path"], data[label]
        ):

            fig.add_subplot(rows, columns, i)
            plt.title(label, size=35)

            plt.imshow(utils.get_image(image_path))

    def plot_images(self, **kwargs):

        data = self._filter_data(**kwargs)
        self._plot_from_data(data=data, **kwargs)

    def get_images(self, **kwargs):

        data = self._filter_data(**kwargs)
        return np.array(
            [utils.get_image(image_path) for image_path in data["image_path"]]
        )

    def create_submission(self):
        pass

    def calculate_statistics(self):
        """
        function to calculate mean value and standard deviation in image cells
        """

        mean = []
        std = []

        for image_path in self.data["image_path"]:
            mean += [utils.get_image(image_path).mean(axis=(0, 1))]
            std += [utils.get_image(image_path).std(axis=(0, 1))]

        self.mean = tuple(np.array(mean).mean(axis=0))
        self.std = tuple(np.array(std).mean(axis=0))

        pickle.dump(
            (self.mean, self.std),
            open(join(cfg.DATA_DIR, "images_mean_std.pkl"), "wb"),
        )

    def get_training_data(self):
        if train_config.stratify:
            train, test = train_test_split(
                self.data,
                test_size=train_config.test_size,
                stratify=self.data["target"],
            )

        else:
            train, test = train_test_split(self.data, test_size=train_config.test_size)

        train.reset_index(inplace=True)
        test.reset_index(inplace=True)

        data = {"train": train, "test": test}

        return data
