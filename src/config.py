import inspect
from os.path import dirname, join

BASIC_DIR = dirname(dirname(inspect.getfile(inspect.currentframe())))

DATA_DIR = join(BASIC_DIR, "data")
TRAIN_DIR = join(DATA_DIR, "train")
TEST_DIR = join(DATA_DIR, "test")

MODEL_DIR = join(BASIC_DIR, "models")
