from imgaug import augmenters as iaa

from src.images.augments import AugmentList, ImgaugAugment

# Augment config
seq = iaa.Sequential(
    [
        iaa.Fliplr(0.5),  # horizontally flip 50% of the images
        iaa.Rotate(rotate=(-90, 90)),
        iaa.Crop(px=50),  # crop images from each side by 0 to 16px (randomly chosen)
        iaa.LinearContrast((0.75, 1.5)),
        iaa.AdditiveGaussianNoise(loc=0, scale=(0.0, 0.05 * 255), per_channel=0.5),
        iaa.Multiply((0.8, 1.2), per_channel=0.2),
        iaa.Sometimes(0.5, iaa.GaussianBlur(sigma=(0, 0.5))),
        iaa.size.Resize(size=128),
    ]
)

augments = AugmentList([ImgaugAugment(seq)])

# Train data config
test_size = 0.2
stratify = True

# Dataloaders config
batch_size = 32
shuffle = {
    "train": True,
    "test": False,
    "valid": False,
}
