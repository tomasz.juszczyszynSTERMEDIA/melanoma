import copy
import cv2
import glob
import ntpath
import numpy as np
import os
import pandas as pd
import random
import torch

from efficientnet_pytorch import EfficientNet
from os.path import join
from tqdm.notebook import tqdm

import src.config as cfg
import src.train_config as train_config


def get_image(image_path):
    """
    returns np array of shape (height, width, channels) of selected picture from
    dataset.
    """
    try:
        img = cv2.imread(join(cfg.DATA_DIR, image_path))
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    except Exception:
        print(image_path)
    return img


def get_sample_images(amount=1):
    """
    returns np array of shape (amount, height, width, channels) of random pictures from
    dataset. Here stage means "train" or "test".
    """
    images_paths = glob.glob(join(cfg.DATA_DIR, "train", "*"))
    image_paths = random.choices(images_paths, k=amount)
    file_names = [
        os.path.splitext(ntpath.basename(image_path))[0] for image_path in image_paths
    ]

    return np.array(
        [get_image(join("train", f"{file_name}.jpg")) for file_name in file_names]
    )


def annotate_hairy():
    """
    function to take all the images from data/hairy/ folder and annotate them
    as hairy in train.csv file.
    """
    hairy = glob.glob(os.path.join(cfg.DATA_DIR, "hairy", "*"))

    hairy_file_names = [
        os.path.splitext(ntpath.basename(image_path))[0] for image_path in hairy
    ]

    len(hairy_file_names)

    data = pd.read_csv(os.path.join(cfg.DATA_DIR, "train.csv"))
    data["annot_hairy"] = 0

    data.loc[data["image_name"].isin(hairy_file_names), "annot_hairy"] = 1

    data.to_csv(os.path.join(cfg.DATA_DIR, "train.csv"), index=False)


def convert_to_file_name(file_path, prefix, sufix):
    """
    function to conver filename to path to the file in train.csv and test.csv
    """
    data = pd.read_csv(file_path)
    data["image_path"] = prefix + data["image_name"] + sufix
    data.to_csv(file_path, index=False)


def load_model(classes=2, version=4):

    nn_version = f"efficientnet-b{version}"
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    model = EfficientNet.from_pretrained(nn_version, num_classes=classes)

    return model.to(device)


def train(model, epochs, dataloaders, loss_function, optimizer):

    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    # remember best model so far
    best_model_wts = copy.deepcopy(model.state_dict())
    best_acc = 0.0

    for epoch in range(epochs):

        print(f"Epoch {epoch + 1} of {epochs}")
        print("-" * 10)

        for phase in ["train", "test"]:

            if phase == "train":
                # set model to training mode
                model.train()
            else:
                # set model to evaluate mode
                model.eval()

            # start counting phase loss
            running_loss = 0.0
            running_corrects = 0

            for batch in tqdm(dataloaders[phase]):

                # load data to device
                inputs = batch[0].to(device)
                labels = batch[1].to(device)

                # reset optimizer
                optimizer.zero_grad()

                # generate outputs of network on batch images and generate loss
                outputs = model(inputs)
                loss = loss_function(outputs, labels)

                # predict the labels of batch images
                _, predictions = torch.max(outputs, 1)

                if phase == "train":
                    loss.backward()
                    optimizer.step()

                # calculate statistics
                running_loss += loss.item() * inputs.size(0)
                batch_acc = (
                    torch.sum(predictions == labels.data).item()
                    / train_config.batch_size
                )
                running_corrects += batch_acc

            epoch_loss = running_loss / len(dataloaders[phase])
            epoch_acc = running_corrects / len(dataloaders[phase])

            print(f"{phase} Loss: {epoch_loss} Acc: {epoch_acc}")

            # deep copy the model
            if phase == "test" and epoch_acc > best_acc:
                best_acc = epoch_acc
                best_model_wts = copy.deepcopy(model.state_dict())

    # load best model weights
    model.load_state_dict(best_model_wts)
